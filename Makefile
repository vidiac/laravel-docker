up:
	docker-compose up -d

build:
	docker-compose up --build -d

down:
	docker-compose down

yarn:
	docker-compose exec node yarn

npm-watch:
	docker-compose exec node npm run watch

npm-build:
	docker-compose exec node npm run build

npm-prod:
	docker-compose exec node npm run prod

run-php:
	docker-compose exec php-fpm bash